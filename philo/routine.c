/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routine.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/05 19:31:46 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Philosophers.h"

static void	eat(t_philo *p)
{
	pthread_mutex_lock(p->a->f[p->n - 1]);
	if (!alive(p, -1))
	{
		unlock(p);
		return ;
	}
	p->left_hand = 1;
	print(p, "has taken a fork");
	if (p->n == p->a->n)
		pthread_mutex_lock(p->a->f[0]);
	else
		pthread_mutex_lock(p->a->f[p->n]);
	p->right_hand = 1;
	if (!alive(p, -1))
	{
		unlock(p);
		return ;
	}
	print(p, "has taken a fork");
	if (alive(p, -1))
		print(p, "is eating");
	ate(p, t(p));
	p->eatc++;
}

static void	food_schedule_2(t_philo *p)
{
	if (p->a->n % 3 != 1)
	{
		if (p->n % 3 == 1 && t(p) % (p->a->el * 3) < p->a->el)
			eat(p);
		else if (p->n % 3 == 2 && t(p) % (p->a->el * 3) < p->a->el * 2 && t(p)
			% (p->a->el * 3) >= p->a->el)
			eat(p);
		else if (!(p->n % 3) && t(p) % (p->a->el * 3) >= p->a->el * 2)
			eat(p);
		return ;
	}
	if (p->n % 3 == 1 && t(p) % (p->a->el * 3) < p->a->el && p->n != p->a->n)
		eat(p);
	else if ((p->n % 3 == 2 || p->n == p->a->n) && t(p) % (p->a->el
			* 3) < p->a->el * 2 && t(p) % (p->a->el * 3) >= p->a->el)
		eat(p);
	else if (!(p->n % 3) && t(p) % (p->a->el * 3) >= p->a->el * 2)
		eat(p);
}

static void	food_schedule(t_philo *p)
{
	if (!(p->a->n % 2))
	{
		if (p->n % 2 && t(p) % (p->a->el * 2) < p->a->el)
			eat(p);
		else if (!(p->n % 2) && t(p) % (p->a->el * 2) >= p->a->el)
			eat(p);
	}
	else
		food_schedule_2(p);
}

static void	routine(t_philo *p)
{
	if (!p->cleared && p->a->en != INT_MIN && p->eatc >= p->a->en)
	{
		p->cleared = 1;
		clearnum(p, 1);
	}
	else if (p->state != 2 && p->ate >= 0 && t(p) >= p->ate + p->a->el
		&& t(p) < p->ate + p->a->el + p->a->s)
	{
		print(p, "is sleeping");
		p->state = 2;
		unlock(p);
	}
	else if (p->ate >= 0 && t(p) < p->ate + p->a->el + p->a->s)
		(void)0;
	else if (!p->left_hand && !p->right_hand)
	{
		if (p->state != 1)
			print(p, "is thinking");
		p->state = 1;
		food_schedule(p);
	}
}

void	*start_routine(void *philo)
{
	t_philo	*p;

	p = philo;
	ate(p, -p->a->el);
	p->cleared = 0;
	p->eatc = 0;
	p->left_hand = 0;
	p->right_hand = 0;
	p->sleep_started = 0;
	p->state = 0;
	gettimeofday(&p->a->t, 0);
	while (1)
	{
		if (!alive(p, -1))
		{
			unlock(p);
			return (0);
		}
		routine(p);
		usleep(500);
	}
}
