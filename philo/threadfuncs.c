/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extra.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/07 20:19:55 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Philosophers.h"

bool	alive(t_philo *p, int newval)
{
	bool	alive;

	pthread_mutex_lock(p->a->alive_lock);
	if (newval != -1)
		p->a->alive = newval;
	alive = p->a->alive;
	pthread_mutex_unlock(p->a->alive_lock);
	return (alive);
}

long	ate(t_philo *p, long newval)
{
	long	ate;

	pthread_mutex_lock(p->ate_lock);
	if (newval != -1)
		p->ate = newval;
	ate = p->ate;
	pthread_mutex_unlock(p->ate_lock);
	return (ate);
}

int	clearnum(t_philo *p, int mod)
{
	int	clearnum;

	pthread_mutex_lock(p->a->clearnum_lock);
	p->a->clearnum += mod;
	clearnum = p->a->clearnum;
	pthread_mutex_unlock(p->a->clearnum_lock);
	return (clearnum);
}
