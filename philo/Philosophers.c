/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/05 19:28:54 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Philosophers.h"

static void	end_loop(t_args *a, t_philo **philo, pthread_t **philosophers)
{
	long	i;

	i = -1;
	while (a && ++i < a->n)
	{
		if (a && a->f && a->f[i])
		{
			pthread_mutex_destroy(a->f[i]);
			free(a->f[i]);
		}
		if (philosophers && philosophers[i])
			free(philosophers[i]);
		if (philo && philo[i] && philo[i]->ate_lock)
		{
			pthread_mutex_destroy(philo[i]->ate_lock);
			free(philo[i]->ate_lock);
		}
		if (philo && philo[i])
			free(philo[i]);
	}
}

static int	end(t_args *a, t_philo **philo, pthread_t **philosophers, int exit)
{
	end_loop(a, philo, philosophers);
	if (a && a->alive_lock)
	{
		pthread_mutex_destroy(a->alive_lock);
		free(a->alive_lock);
	}
	if (a && a->clearnum_lock)
	{
		pthread_mutex_destroy(a->clearnum_lock);
		free(a->clearnum_lock);
	}
	if (a && a->print_lock)
	{
		pthread_mutex_destroy(a->print_lock);
		free(a->print_lock);
	}
	if (philo)
		free(philo);
	if (philosophers)
		free(philosophers);
	if (a && a->f)
		free(a->f);
	if (a)
		free(a);
	return (exit);
}

static int	create_philo_loop(t_args *a, t_philo **philo, pthread_t **philo2)
{
	int	i;

	pthread_mutex_init(a->alive_lock, 0);
	pthread_mutex_init(a->clearnum_lock, 0);
	pthread_mutex_init(a->print_lock, 0);
	i = -1;
	while (++i < a->n)
	{
		philo2[i] = malloc(sizeof(pthread_t));
		a->f[i] = malloc(sizeof(pthread_mutex_t));
		philo[i] = malloc(sizeof(t_philo));
		if (!philo2[i] || !a->f[i] || !philo[i])
			return (end(a, philo, philo2, EXIT_FAILURE));
		philo[i]->a = a;
		philo[i]->n = i + 1;
		pthread_mutex_init(a->f[i], 0);
	}
	i = -1;
	while (++i < a->n)
		pthread_create(philo2[i], 0, monitor, philo[i]);
	while (i--)
		pthread_join(*philo2[i], 0);
	return (end(a, philo, philo2, EXIT_SUCCESS));
}

static int	create_philo(t_args *a)
{
	t_philo		**philo;
	pthread_t	**philosophers;

	philo = malloc(8 * a->n);
	philosophers = malloc(8 * a->n);
	if (!philo || !philosophers)
		return (end(a, philo, philosophers, EXIT_FAILURE));
	return (create_philo_loop(a, philo, philosophers));
}

int	main(int argc, char **argv)
{
	t_args	*a;

	a = malloc(sizeof(t_args));
	if (!a)
		return (end(a, 0, 0, EXIT_FAILURE));
	a->alive = 1;
	a->alive_lock = malloc(sizeof(pthread_mutex_t));
	a->clearnum = 0;
	a->clearnum_lock = malloc(sizeof(pthread_mutex_t));
	a->el = ft_atoi(argv[3]);
	a->en = INT_MIN;
	a->n = ft_atoi(argv[1]);
	a->l = ft_atoi(argv[2]);
	a->print = 1;
	a->print_lock = malloc(sizeof(pthread_mutex_t));
	a->s = ft_atoi(argv[4]);
	if (argc == 6)
		a->en = ft_atoi(argv[5]);
	a->f = malloc(a->n * 8);
	if (!a->f || !a->alive_lock || !a->clearnum_lock || !a->print_lock
		|| a->n < 1 || a->l < 1 || a->s < 1 || (argc == 6 && a->en < 1))
		return (end(a, 0, 0, EXIT_FAILURE));
	return (create_philo(a));
}
