/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/05 19:28:54 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Philosophers.h"

static void	*monitor_loop(t_philo *p)
{
	while (1)
	{
		if (((ate(p, -1) < 0 && t(p) > p->a->l)) || (ate(p, -1) >= 0 && t(p)
				- ate(p, -1) > p->a->l))
		{
			pthread_mutex_lock(p->a->print_lock);
			if (p->a->print)
				printf("%06lums: %03d died\n", t(p), p->n);
			p->a->print = 0;
			pthread_mutex_unlock(p->a->print_lock);
			alive(p, 0);
			return (0);
		}
		else if (clearnum(p, 0) == p->a->n || !alive(p, -1))
		{
			alive(p, 0);
			return (0);
		}
		usleep(500);
	}
}

void	*monitor(void *philo)
{
	t_philo		*p;
	pthread_t	*routine;

	p = philo;
	p->ate_lock = malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(p->ate_lock, 0);
	routine = malloc(sizeof(pthread_t));
	if (!routine)
		return (0);
	pthread_create(routine, 0, start_routine, philo);
	pthread_detach(*routine);
	gettimeofday(&p->a->t, 0);
	monitor_loop(p);
	free(routine);
	return (0);
}
