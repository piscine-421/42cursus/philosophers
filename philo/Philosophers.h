/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/05 19:28:58 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H
# include <limits.h>
# include <pthread.h>
# include <stdatomic.h>
# include <stdbool.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/time.h>
# include <unistd.h>

typedef struct s_args
{
	atomic_bool		alive;
	pthread_mutex_t	*alive_lock;
	atomic_int		clearnum;
	pthread_mutex_t	*clearnum_lock;
	long			el;
	long			en;
	pthread_mutex_t	**f;
	long			l;
	int				n;
	atomic_bool		print;
	pthread_mutex_t	*print_lock;
	long			s;
	struct timeval	t;
}					t_args;

typedef struct s_philo
{
	struct s_args	*a;
	atomic_long		ate;
	pthread_mutex_t	*ate_lock;
	bool			cleared;
	long			eatc;
	bool			left_hand;
	int				n;
	bool			right_hand;
	size_t			sleep_started;
	int				state;
}					t_philo;

bool				alive(t_philo *p, int newval);
long				ate(t_philo *p, long newval);
int					clearnum(t_philo *p, int mod);
int					ft_atoi(const char *str);
void				*monitor(void *philo);
void				print(t_philo *p, char *str);
long				t(t_philo *p);
void				*start_routine(void *philo);
void				unlock(t_philo *p);

#endif
