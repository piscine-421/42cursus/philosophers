/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routine.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/05 19:31:46 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Philosophers.h"

long	ate(t_philo *p, long newval, bool update)
{
	long	ate;

	sem_wait(p->ate_lock);
	if (update)
		p->ate = newval;
	ate = p->ate;
	sem_post(p->ate_lock);
	return (ate);
}

int	geti(t_mon *m, int mod)
{
	int	i;

	sem_wait(m->i_lock);
	m->i += mod;
	i = m->i;
	sem_post(m->i_lock);
	return (i);
}

bool	stop(t_mon *m, int newval)
{
	bool	stop;

	sem_wait(m->stop_lock);
	if (newval != -1)
		m->stop = newval;
	stop = m->stop;
	sem_post(m->stop_lock);
	return (stop);
}

int	status(t_mon *m, int newval)
{
	int	status;

	sem_wait(m->a->status_lock);
	if (newval != -1)
		m->a->status = newval;
	status = m->a->status;
	sem_post(m->a->status_lock);
	return (status);
}
