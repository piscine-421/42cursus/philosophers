/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/05 19:28:58 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H
# include <limits.h>
# include <pthread.h>
# include <semaphore.h>
# include <signal.h>
# include <stdatomic.h>
# include <stdbool.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/time.h>
# include <sys/wait.h>
# include <unistd.h>

typedef struct s_args
{
	sem_t			*done;
	long			c;
	long			el;
	long			l;
	long			n;
	sem_t			*print;
	sem_t			*sem;
	long			s;
	atomic_bool		status;
	sem_t			*status_lock;
	struct timeval	t;
}					t_args;

typedef struct s_mon
{
	struct s_args	*a;
	pthread_t		*chkeat;
	atomic_int		i;
	sem_t			*i_lock;
	int				*pid;
	atomic_bool		stop;
	sem_t			*stop_lock;
}					t_mon;

typedef struct s_philo
{
	struct s_args	*a;
	atomic_long		ate;
	sem_t			*ate_lock;
	long			eatc;
	size_t			holding;
	pthread_t		*lifemon;
	long			n;
	size_t			sleep_started;
	int				state;
}					t_philo;

long				ate(t_philo *p, long newval, bool update);
void				create_monitor(t_mon *m, t_philo **philo);
void				end(t_mon *m, t_philo **philo);
int					ft_atoi(const char *str);
int					geti(t_mon *m, int mod);
void				*init_philo(void *philo);
void				print(t_philo *p, char *str);
int					status(t_mon *m, int newval);
bool				stop(t_mon *m, int newval);
long				t(t_philo *p);
void				unlock(t_philo *p);

#endif
