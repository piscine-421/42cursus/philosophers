/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/05 19:28:54 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Philosophers.h"

static void	*ft_chkeat(void *monitor)
{
	t_mon	*m;

	m = monitor;
	while (1)
	{
		status(m, 1);
		sem_wait(m->a->done);
		sem_post(m->a->done);
		status(m, 0);
		usleep(500);
	}
}

static void	*ft_chkeat_monitor(void *monitor)
{
	t_mon	*m;
	int		time;

	m = monitor;
	status(m, 0);
	time = 0;
	pthread_create(m->chkeat, 0, ft_chkeat, m);
	while (1)
	{
		if (status(m, -1))
			time++;
		else
			time = 0;
		if (time >= 5)
		{
			stop(m, 1);
			sem_close(m->a->done);
			sem_unlink("done");
			return (0);
		}
		usleep(500);
	}
}

static void	create_monitor_loop(t_mon *m, t_philo **philo)
{
	int	pid;

	while (geti(m, 1) < m->a->n)
	{
		pid = fork();
		if (!pid)
			init_philo(philo[geti(m, 0)]);
	}
	while (1)
	{
		if (stop(m, -1))
		{
			while (geti(m, 0))
			{
				geti(m, -1);
				kill(m->pid[geti(m, 0)], SIGINT);
			}
			break ;
		}
		usleep(500);
	}
}

void	create_monitor(t_mon *m, t_philo **philo)
{
	pthread_t	*chkeat_monitor;

	chkeat_monitor = malloc(sizeof(pthread_t));
	m->chkeat = malloc(sizeof(pthread_t));
	m->i = -1;
	m->pid = malloc(sizeof(int) * m->a->n);
	m->stop = 0;
	if (!chkeat_monitor || !m->chkeat || !m->pid)
	{
		free(chkeat_monitor);
		end(m, philo);
	}
	pthread_create(chkeat_monitor, 0, ft_chkeat_monitor, m);
	create_monitor_loop(m, philo);
	free(chkeat_monitor);
	end(m, philo);
}
