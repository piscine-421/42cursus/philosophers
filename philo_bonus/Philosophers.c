/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Philosophers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/05 19:28:54 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Philosophers.h"

static void	create_philo_loop(t_mon *m, t_philo **philo)
{
	int	i;

	i = -1;
	while (++i < m->a->n)
	{
		philo[i] = malloc(sizeof(t_philo));
		if (!philo[i])
			end(m, philo);
		philo[i]->ate_lock = sem_open("ate_lock", O_CREAT, 777, 1);
		philo[i]->lifemon = malloc(sizeof(pthread_t));
		if (!philo[i]->lifemon || !philo[i]->ate_lock)
			end(m, philo);
		philo[i]->a = m->a;
		philo[i]->n = i + 1;
	}
}

static void	create_philo(t_mon *m)
{
	t_philo		**philo;

	sem_unlink("i_lock");
	sem_unlink("print");
	sem_unlink("sem");
	sem_unlink("status_lock");
	sem_unlink("stop_lock");
	philo = malloc(8 * m->a->n);
	if (!philo)
		end(m, philo);
	create_philo_loop(m, philo);
	m->a->done = sem_open("done", O_CREAT, 777, m->a->n);
	m->a->print = sem_open("print", O_CREAT, 777, 1);
	m->a->sem = sem_open("sem", O_CREAT, 777, m->a->n);
	m->a->status_lock = sem_open("status_lock", O_CREAT, 777, 1);
	m->i_lock = sem_open("i_lock", O_CREAT, 777, 1);
	m->stop_lock = sem_open("stop_lock", O_CREAT, 777, 1);
	if (!m->a->done || !m->a->print || !m->a->sem || !m->a->status_lock
		|| !m->i_lock || !m->stop_lock)
		end(m, philo);
	gettimeofday(&m->a->t, 0);
	create_monitor(m, philo);
}

static void	end_loop(t_mon *m, t_philo **philo)
{
	int	i;

	i = -1;
	while (m && m->a && m->a->n && ++i < m->a->n)
	{
		if (philo && philo[i] && philo[i]->ate_lock)
			sem_close(philo[i]->ate_lock);
		if (philo && philo[i] && philo[i]->lifemon)
			free(philo[i]->lifemon);
		if (philo && philo[i])
			free(philo[i]);
	}
	if (philo)
		free(philo);
	if (m && m->i_lock)
		sem_close(m->i_lock);
}

void	end(t_mon *m, t_philo **philo)
{
	end_loop(m, philo);
	if (m && m->a && m->a->status_lock)
		sem_close(m->a->status_lock);
	if (m && m->a && m->stop_lock)
		sem_close(m->stop_lock);
	if (m && m->a && m->a->sem)
		sem_close(m->a->sem);
	if (m && m->a && m->a->print)
		sem_close(m->a->print);
	if (m && m->a)
		free(m->a);
	if (m && m->chkeat)
		free(m->chkeat);
	if (m && m->pid)
		free(m->pid);
	if (m)
		free(m);
	sem_unlink("ate_lock");
	sem_unlink("i_lock");
	sem_unlink("print");
	sem_unlink("sem");
	sem_unlink("status_lock");
	sem_unlink("stop_lock");
	exit(0);
}

int	main(int argc, char **argv)
{
	t_mon	*m;

	m = malloc(sizeof(t_mon));
	if (!m || (argc != 5 && argc != 6))
		end(m, 0);
	m->a = malloc(sizeof(t_args));
	if (!m->a)
		end(m, 0);
	m->a->n = ft_atoi(argv[1]);
	m->a->l = ft_atoi(argv[2]);
	m->a->el = ft_atoi(argv[3]);
	m->a->s = ft_atoi(argv[4]);
	m->a->c = LONG_MIN;
	if (argc == 6)
		m->a->c = ft_atoi(argv[5]);
	if (m->a->n < 1 || m->a->l < 1 || m->a->s < 1 || (argc == 6 && m->a->c < 1))
	{
		printf("Invalid values.\n");
		free(m->a);
		return (EXIT_FAILURE);
	}
	sem_unlink("ate_lock");
	sem_unlink("done");
	create_philo(m);
}
