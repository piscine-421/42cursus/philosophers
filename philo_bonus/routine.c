/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routine.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/05 19:31:46 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Philosophers.h"

static void	eat(t_philo *p)
{
	sem_wait(p->a->sem);
	p->holding += 1;
	print(p, "has taken a fork");
	if (p->a->n != 1)
	{
		sem_wait(p->a->sem);
		p->holding += 1;
		print(p, "has taken a fork");
		print(p, "is eating");
		ate(p, t(p), 1);
		p->eatc++;
	}
}

static void	food_schedule(t_philo *p)
{
	if (!(p->a->n % 2))
	{
		if (p->n % 2 && t(p) % (p->a->el * 2) < p->a->el)
			eat(p);
		else if (!(p->n % 2) && t(p) % (p->a->el * 2) >= p->a->el)
			eat(p);
		return ;
	}
	if (p->a->n % 3 != 1)
	{
		if (p->n % 3 == 1 && t(p) % (p->a->el * 3) < p->a->el)
			eat(p);
		else if (p->n % 3 == 2 && t(p) % (p->a->el * 3) < p->a->el * 2 && t(p)
			% (p->a->el * 3) >= p->a->el)
			eat(p);
		else if (!(p->n % 3) && t(p) % (p->a->el * 3) >= p->a->el * 2)
			eat(p);
		return ;
	}
	if (p->n % 3 == 1 && t(p) % (p->a->el * 3) < p->a->el && p->n != p->a->n)
		eat(p);
	else if ((p->n % 3 == 2 || p->n == p->a->n) && t(p) % (p->a->el
			* 3) < p->a->el * 2 && t(p) % (p->a->el * 3) >= p->a->el)
		eat(p);
}

static void	routine2(t_philo *p)
{
	if (!p->holding)
	{
		if (p->state != 1)
			print(p, "is thinking");
		p->state = 1;
		if (!(p->n % 3) && t(p) % (p->a->el * 3) >= p->a->el * 2)
			eat(p);
		else
			food_schedule(p);
	}
}

static void	routine(t_philo *p)
{
	if (((ate(p, 0, 0) < 0 && t(p) > p->a->l)) || (ate(p, 0, 0) >= 0 && t(p)
			- ate(p, 0, 0) > p->a->l))
	{
		p->a->c = LONG_MIN;
		sem_wait(p->a->done);
		unlock(p);
		sem_wait(p->a->print);
		printf("%06lums: %03lu died\n", t(p), p->n);
	}
	else if (p->a->c != LONG_MIN && p->eatc >= p->a->c)
	{
		p->a->c = LONG_MIN;
		sem_wait(p->a->done);
	}
	else if (p->state != 2 && ate(p, 0, 0) >= 0 && t(p) >= ate(p, 0, 0)
		+ p->a->el && t(p) < ate(p, 0, 0) + p->a->el + p->a->s)
	{
		unlock(p);
		print(p, "is sleeping");
		p->state = 2;
	}
	else if (ate(p, 0, 0) >= 0 && t(p) < ate(p, 0, 0) + p->a->el + p->a->s)
		(void)0;
	else
		routine2(p);
}

void	*init_philo(void *philo)
{
	t_philo	*p;

	p = philo;
	ate(p, -p->a->el, 1);
	p->eatc = 0;
	p->holding = 0;
	p->sleep_started = 0;
	p->state = 0;
	while (t(p) < 0)
		(void)0;
	while (1)
	{
		routine(p);
		usleep(500);
	}
}
