/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extra.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 1970/01/01 00:00:00 by lcouturi          #+#    #+#             */
/*   Updated: 2024/01/07 20:19:55 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Philosophers.h"

int	ft_atoi(const char *str)
{
	int	i;
	int	sign;
	int	returned;

	i = 0;
	returned = 0;
	if (!str)
		return (returned);
	sign = 1;
	while ((str[i] > 8 && str[i] < 14) || str[i] == 32)
		i++;
	if (str[i] == 43 || str[i] == 45)
	{
		if (str[i] == 45)
			sign = -1;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		returned *= 10;
		returned += str[i] - 48;
		i++;
	}
	returned *= sign;
	return (returned);
}

void	print(t_philo *p, char *str)
{
	sem_wait(p->a->print);
	printf("%06lums: %03lu %s\n", t(p), p->n, str);
	sem_post(p->a->print);
}

long	t(t_philo *p)
{
	t_args			a;
	struct timeval	t;
	unsigned long	time;

	a = *p->a;
	gettimeofday(&t, 0);
	time = ((t.tv_sec - a.t.tv_sec) * 1000000 + t.tv_usec - a.t.tv_usec) / 1000;
	return (time - 10);
}

void	unlock(t_philo *p)
{
	while (p->holding)
	{
		sem_post(p->a->sem);
		p->holding -= 1;
	}
}
